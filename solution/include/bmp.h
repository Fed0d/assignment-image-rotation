#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdio.h>

#include "image.h"

enum read_status {
	READ_OK = 0,
	READ_INVALID_BF_TYPE_ERROR,
	READ_INVALID_BIT_COUNT_ERROR,
	READ_INVALID_HEADER_ERROR,
	READ_IMAGE_ERROR,
	READ_UNEXPECTED_ERROR
};

enum read_status from_bmp( FILE *in, struct image *image );

enum write_status {
	WRITE_OK = 0,
	WRITE_HEADER_ERROR,
	WRITE_IMAGE_ERROR,
	WRITE_UNEXPECTED_ERROR
};

enum write_status to_bmp( FILE *out, const struct image *image );

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
