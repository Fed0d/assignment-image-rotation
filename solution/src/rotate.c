#include "rotate.h"

#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

static struct pixel* get_pixel( struct pixel* pixels, uint32_t x, uint32_t y, uint64_t width ) {
	return pixels + (y * width + x);
} 

struct image rotation( struct image const original ) {
	struct image rotated_image = { 0 };

	if ( original.data ) {
		const uint32_t width = original.width;
		const uint32_t height = original.height;
		
		rotated_image = image_create(height, width );
		
		if ( rotated_image.data ) {
			for ( size_t j = 0; j < height; ++j ) {
				for ( size_t i = 0; i < width; ++i ) {
					const uint32_t x = i;
					const uint32_t y = j;
				
					const uint32_t x_r = height - j - 1;
					const uint32_t y_r = i;
					
					*get_pixel( rotated_image.data, x_r, y_r, height ) = *get_pixel( original.data, x, y, width );
				}
			}
		}
	}
	
	return rotated_image;
}

