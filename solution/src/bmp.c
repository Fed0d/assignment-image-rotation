
#include "bmp.h"

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#define BF_TYPE 0x4D42
#define BITS 24

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status bmp_header_check( struct bmp_header header ) {
	if ( header.bfType != BF_TYPE ) {
		return READ_INVALID_BF_TYPE_ERROR;
	}
	
	if ( header.biBitCount != BITS ) {
		return READ_INVALID_BIT_COUNT_ERROR;
	}

	return READ_OK;
}

static enum read_status bmp_header_read( struct bmp_header *header, FILE *in ) {
	if ( fread( header, sizeof(struct bmp_header), 1, in ) != 1 )
		return READ_INVALID_HEADER_ERROR;
		
	return bmp_header_check( *header );
}

static size_t calc_padding( const uint64_t width ) {
	return width % 4;
}

static enum read_status image_read_pixels( FILE *in, struct image *image ) {
	if ( image->data ) {
		const uint64_t width = image->width;
		const uint64_t height = image->height;
		
		const size_t padding = calc_padding( width );
		
		for (size_t i = 0; i < height; ++i) {
			if ( fread( image->data + i * width, sizeof(struct pixel), width, in ) != width )
				return READ_IMAGE_ERROR;
			
			if ( fseek( in, padding, SEEK_CUR ) )
				return READ_IMAGE_ERROR;
		}
		
		return READ_OK;
	}
	else
		return READ_UNEXPECTED_ERROR;
}

enum read_status from_bmp( FILE *in, struct image *image ) {
	struct bmp_header header = {0};
	enum read_status status = bmp_header_read( &header, in );
	
	if ( status != READ_OK ) {
		return status;
	}
	
	*image = image_create( header.biWidth, header.biHeight );
		
	return image_read_pixels( in, image );
}

static struct bmp_header bmp_header_from_image( const struct image *image ) {
	const size_t padding = calc_padding( image->width );

	const uint32_t image_size = sizeof(struct pixel) * image->width * image->height;
	const uint32_t file_size = sizeof(struct bmp_header) + image_size + padding * image->height; 

	return (struct bmp_header) {
		.bfType = BF_TYPE,
		.bfileSize = file_size,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = 40,
		.biWidth = image->width,
		.biHeight = image->height,
		.biPlanes = 1,
		.biBitCount = BITS,
		.biCompression = 0,
		.biSizeImage = image_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0
	};
}

static enum write_status bmp_header_write( FILE *out, struct bmp_header *header ) {
	if ( fwrite( header, sizeof(struct bmp_header), 1, out ) != 1 )
		return WRITE_HEADER_ERROR;
	
	return WRITE_OK;
}

static enum write_status image_write_pixels( FILE *out, const struct image *image ) {
	const uint64_t width = image->width;
	const uint64_t height = image->height;

	const size_t padding = calc_padding( width );
	
	const uint8_t arr[4] = { 0, 0, 0, 0 };
	
	for ( size_t i = 0; i < height; ++i ) {
		if ( fwrite( image->data + i * width, sizeof(struct pixel), width, out ) != width )
			return WRITE_IMAGE_ERROR;
		
		if ( fwrite( arr, 1, padding, out ) != padding )
			return WRITE_IMAGE_ERROR;
	}
	
	return WRITE_OK;
}

enum write_status to_bmp(FILE *out, const struct image *image) {
	struct bmp_header header = bmp_header_from_image( image );
	
	enum write_status status = bmp_header_write( out, &header );
	
	if ( status != WRITE_OK )
		return WRITE_OK;
	
	return image_write_pixels( out, image );
}
