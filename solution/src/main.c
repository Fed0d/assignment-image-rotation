#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"

#define FOPEN_EXIT_CODE 1
#define FROM_BMP_EXIT_CODE 2
#define FCLOSE_EXIT_CODE 3
#define IMAGE_ROTATE_EXIT_CODE 4
#define TO_BMP_EXIT_CODE 5

static char* msg_from_read_status( enum read_status status ) {
	if ( status == READ_INVALID_BF_TYPE_ERROR )
		return "invalid bmp header";
	else if ( status == READ_INVALID_BIT_COUNT_ERROR )
		return "invalid bit count";
	else if ( status == READ_INVALID_HEADER_ERROR )
		return "invalid header";
	else if ( status == READ_IMAGE_ERROR )
		return "invaid image data";
	else
		return "unexpected error";
}

static void image_read_by_filename( const char *filename, struct image *image ) {
	FILE *input_file = fopen( filename, "rb" );

	if ( !input_file ) {
		fprintf( stderr, "Cannot open file: %s\n", filename );
		exit( FOPEN_EXIT_CODE );
	}
	
	enum read_status status = from_bmp( input_file, image );
	
	if ( status != READ_OK ) {
		image_destroy( image );
	
		const char* msg = msg_from_read_status( status );
		
		fprintf( stderr, "Reading failed because of %s\n", msg );
		
		if ( ferror( input_file ) ) {
			perror( "More information: " );
		}
			
		exit( FROM_BMP_EXIT_CODE );
	}
	
	if ( fclose( input_file ) == EOF ) {
		image_destroy( image );
	
		fprintf( stderr, "Cannot close file: %s\n", filename );		
		exit( FCLOSE_EXIT_CODE );
	}
}

static void rotate_image( struct image *source, struct image* destination ) {
	if ( source && destination ) {
		*destination = rotation( *source );
		
		if ( destination->data == NULL ) {
			image_destroy( source );
			
			fprintf( stderr, "Cannot create rotated image\n" );
			exit( IMAGE_ROTATE_EXIT_CODE );
		}
	}
}

static char* msg_from_write_status( enum write_status status ) {
	if ( status == WRITE_HEADER_ERROR )
		return "errors with writing header";
	else if ( status == WRITE_IMAGE_ERROR )
		return "errors with writing image data";
	else
		return "unexpected error";
}

static void image_write_by_filename( const char *filename, struct image *image ) {
	FILE *output_file = fopen( filename, "wb" );
	
	if ( !output_file ) {
		image_destroy( image );
		fprintf( stderr, "Cannot open file: %s\n", filename );
		exit( FOPEN_EXIT_CODE );
	} 
	
	enum write_status status = to_bmp( output_file, image );
	
	if ( status != WRITE_OK ) {
		image_destroy( image );
		
		const char* msg = msg_from_write_status(status);
		
		fprintf( stderr, "Writing failed because of %s\n", msg );
		
		if ( ferror( output_file ) )
			perror( "More information: " );
		
		exit( TO_BMP_EXIT_CODE );
	}
	
	if ( fclose( output_file ) == EOF ) {
		image_destroy( image );
		fprintf( stderr, "Cannot close file: %s\n", filename );
		exit( FCLOSE_EXIT_CODE );
	}
}


int main( int argc, char** argv ) {
	(void) argc; (void) argv; // supress 'unused parameters' warning
	
	struct image image = { 0 };
	char* filename = argv[1];
	
	image_read_by_filename( filename, &image );
	
	struct image rotated_image = { 0 };
	
	rotate_image( &image, &rotated_image );
	
	image_destroy( &image );
	
	filename = argv[2];
	
	image_write_by_filename( filename, &rotated_image );
	
	image_destroy( &rotated_image );
	
	return 0;
}

